(function(){
	var app = {
		module: {

			string: {
				declination: function (number, titles){
					cases = [2, 0, 1, 1, 1, 2];
					return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
				}
			},


			iosFix: {

				removeIOSRubberEffect: function(el) {

					var $body = $('body'),
						t,
						st,
						wh,
						h,
						y0 = 0,
						y1 = 0,
						yd = 0;

					$body
						.on('touchstart', el, function (e) {
							var $t = $(this);

							y0 = e.pageY || e.originalEvent.targetTouches[0].pageY;

							t = $t.offset().top;
							st = $t.scrollTop();
							wh = $t[0].scrollHeight;
							h = $t.outerHeight(true);
						})
						.on('touchmove', el, function (e) {
							var $t = $(this);

							if ( y0 !== 0){
								y1 = e.pageY || e.originalEvent.targetTouches[0].pageY;
								yd = (( y1 - y0 ) > 0 ) ? (-1) : ( ( y1 - y0 ) < 0 ? (1) : 0);
							}

							t = $t.offset().top;
							st = $t.scrollTop();
							wh = $t[0].scrollHeight;
							h = $t.outerHeight(true);

							if ( st <= 0 && yd < 0){
								$t.scrollTop(st);
								e.preventDefault();
								$body.on('touchmove.popupbg', function(e){ e.preventDefault(); });
							} else if ( st >= (wh - h) && yd > 0){
								$t.scrollTop(wh - h);
								$body.on('touchmove.popupbg', function(e){ e.preventDefault(); });
							} else {
								$body.off('touchmove.popupbg');
							}

						})
						.on('touchend', el, function (e) {
							$body.off('touchmove.popupbg');
						});
				},

				init: function(){

					if ( !!Modernizr ){

						Modernizr.addTest('ipad', function () {
							return !!navigator.userAgent.match(/iPad/i);
						}).addTest('iphone', function () {
							return !!navigator.userAgent.match(/iPhone/i);
						}).addTest('ipod', function () {
							return !!navigator.userAgent.match(/iPod/i);
						}).addTest('ios', function () {
							return (Modernizr.ipad || Modernizr.ipod || Modernizr.iphone);
						});
					}

					if ( Modernizr.ipad || Modernizr.ipod || Modernizr.iphone ){

						self.iosFix.removeIOSRubberEffect('.nav');
						self.iosFix.removeIOSRubberEffect('.popup');

					}

				}
			},


			setMenuPosition: {

				pos: function ($el) {
					$el.scrollToFixed({
						marginTop: 0,
						zIndex: 99,
						removeOffsets: true
					});
				},

				init: function (el) {
					var $el = $(el);
					self.setMenuPosition.pos($el);
					$(window).resize(function () {
						$el.trigger('detach.ScrollToFixed');
						self.setMenuPosition.pos($el);
					});
				}
			},

			checkForm: function(form){
				var $el = $(form),
					wrong = false;

				$el.find('[data-required]').each(function(){
					var $t = $(this),
						type = $t.data('required'),
						$wrap = $t.closest('.form__field'),
						$mes = $wrap.find('.form__field-message'),
						val = $.trim($t.val()),
						errMes = '',
						rexp = /.+$/igm;

					$wrap.removeClass('error success');
					$mes.html('');

					if ( $t.attr('type') == 'checkbox' && !$t.is(':checked') ) {
						val = false;
					} else if ( /^(#|\.)/.test(type) ){
						if ( val !== $(type).val() || !val ) val = false;
					} else if ( /^(name=)/.test(type) ){
						if ( val !== $('['+type+']').val() || !val ) val = false;
					} else if ( $t.attr('type') == 'radio'){
						var name =  $t.attr('name');
						if ( $('input[name='+name+']:checked').length < 1 ) val = false;
					} else {
						switch (type) {
							case 'number':
								rexp = /^\d+$/i;
								errMes = 'Поле должно содержать только числовые символы';
								break;
							case 'phone':
								rexp = /[\+]\d\s[\(]\d{3}[\)]\s\d{3}\s\d{2}\s\d{2}/i;
								break;
							case 'letter':
								rexp = /^[A-zА-яЁё]+$/i;
								break;
							case 'rus':
								rexp = /^[А-яЁё]+$/i;
								break;
							case 'email':
								rexp = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
								errMes = 'Проверьте корректность email';
								break;
							case 'password':
								rexp = /^(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
								errMes = 'Слишком простой пароль';
								break;
							default:
								rexp = /.+$/igm;
						}
					}


					if ( !rexp.test(val) || val == 'false' || !val ){
						wrong = true;
						$wrap.addClass('error');

						if (!val) errMes = 'Поле не заполнено';

						if ( errMes ){

							$mes.html(errMes);
						}

					} else {
						$wrap.addClass('success');

					}

				});


				return !wrong;

			},

			viewPort: {

				current: '',

				data: {
					'0': function(){
						self.viewPort.current = 0;
						console.info('-----mobile-----');
					},
					'640': function(){
						self.viewPort.current = 1;
						console.info('-----tabletVer-----');
					},
					'960': function(){
						self.viewPort.current = 2;
						console.info('-----tabletHor-----');
					},
					'1200': function(){
						self.viewPort.current = 3;
						console.info('-----desktop-----');
					},
					'1500': function(){
						self.viewPort.current = 4;
						console.info('-----desktop HD-----');
					},
					'1800': function(){
						self.viewPort.current = 5;
						console.info('-----full HD-----');
					}
				},

				init: function(data){
					var points = data || self.viewPort.data;
					if ( points ){
						points['Infinity'] = null;
						var sbw = scrollBarWidth(), curPoint = null;
						var ww = $(window).width() + sbw;
						checkCurrentViewport();
						$(window).on('resize', function(){
							ww = $(window).width() + sbw;
							checkCurrentViewport();
						});
					}

					function checkCurrentViewport(){
						var pp = 0, pc = null;
						$.each(points, function(point, callback){
							if ( point > ww ){
								if ( pp !== curPoint ) {
									curPoint = pp;
									pc();
								}
								return false;
							}
							pp = point; pc = callback;
						});
					}

					function scrollBarWidth(){
						var scrollDiv = document.createElement('div');
						scrollDiv.className = 'scroll_bar_measure';
						$(scrollDiv).css({
							width: '100px',
							height: '100px',
							overflow: 'scroll',
							position: 'absolute',
							top: '-9999px'
						});
						document.body.appendChild(scrollDiv);
						sbw = scrollDiv.offsetWidth - scrollDiv.clientWidth;
						document.body.removeChild(scrollDiv);
						return sbw;
					}

				}
			},



			tooltip: {
				init: function(){
					var $tooltip = $('.tooltip');

					if ( !$tooltip.size() ){
						$tooltip = $('<div/>', {'class': 'tooltip'}).appendTo('body');
					}

					$('body').on('mouseover', '[data-tooltip]', function(){
						var $t = $(this);
						var text = $t.attr('data-tooltip');
						$tooltip.html('<span class="tooltip-inner">'+text+'</span>').stop(true, true).delay(1000).addClass('show');

						var top = $t.offset().top + $t.outerHeight();
						var left = $t.offset().left + ($t.outerWidth()/2);
						$tooltip.css({top: top, left: left});

					}).on('mouseleave', '[data-tooltip]', function(){
						$tooltip.removeClass('show');
					});


				}
			},

			preLoad: {

				start: function(el){
					var $el = $(el);
					$el.addClass('loading preloader-wrap').append('<div class="preloader"><div class="preloader-i"></div></div>');
				},

				stop: function(el){
					var $el = $(el);
					$el.removeClass('loading preloader-wrap').find('.preloader').remove();
				},

				init: function(){
					$('body').on('click', function(e){
						if ( $(this).hasClass('loading')){
							e.preventDefault();
							e.stopPropagation();
							return false;
						}
					}).on('click', '.preloader', function(e){
						e.preventDefault();
						e.stopPropagation();
						return false;
					})
				}

			},

			browserDetect: {
				data: {
					template: 	'<div class="popup popup_browser">'
					+				'<div class="popup__inner">'
					+					'<div class="popup__layout">'
					+						'<div class="browser-detect__title"></div>'
					+						'<div class="browser-detect__subtitle"></div>'
					+						'<div class="browser-detect__message"></div>'
					+						'<div class="browser-detect__browsers">'
					+							'<a href="https://www.google.ru/chrome/browser/desktop/" class="browser-detect__item" target="_blank">Chrome</a> '
					+							'<a href="http://windows.microsoft.com/ru-ru/internet-explorer/download-ie" class="browser-detect__item" target="_blank">Internet Explorer</a> '
					+							'<a href="http://www.opera.com/" class="browser-detect__item" target="_blank">Opera</a> '
					+							'<a href="https://www.mozilla.org/ru/firefox/new/" class="browser-detect__item" target="_blank">Firefox</a> '
					+							( ( jQBrowser.platform == 'mac' || jQBrowser.platform == 'iphone' || jQBrowser.platform == 'ipad' )? '<a href="http://www.apple.com/ru/safari/" class="browser-detect__item" target="_blank">Safari</a> ' : '')
					+						'</div>'
					+				    '</div>'
					+				'</div>'
					+				'<div class="popup__overlay"></div>'
					+			'</div>',
					check: [
						['msie', '10', '', 'desktop'],
						['chrome', '42', 'win', 'desktop'],
						['firefox', '40'],
						['opera', '31'],
						['safari', '', 'win'],
						['safari', '8', 'mac'],
						['safari', '8', '', 'mobile']
					],
					onInit: function(){
						$('body').addClass('overflow_hidden');
					}
//				, timeout: 9999
				},

				init: function(){
					$.browserDetect(self.browserDetect.data);
				}
			},


			popup: {
				create: function(popup){
					var pref = popup.indexOf('#') == 0  ? 'id' : 'class';
					var name = popup.replace(/^[\.#]/,'');
					var $popup = $('<div class="popup">'
						+			'<div class="popup__inner">'
						+				'<div class="popup__layout">'
						+					'<div class="popup__close"></div>'
						+					'<div class="popup__content"></div>'
						+				'</div>'
						+			'</div>'
						+			'<div class="popup__overlay"></div>'
						+		'</div>').appendTo('body');

					if ( pref == 'id'){
						$popup.attr(pref, name);
					} else {
						$popup.addClass(name);
					}

					return $popup;
				},

				open: function(popup, html){
					var $popup = $(popup);
					if (!$popup.size()){
						$popup = self.popup.create(popup);
					}
					if( html ){
						$popup.find('.popup__content').html(html);
					}
					$('body').addClass('overflow_hidden');
					return $popup.show();
				},

				close: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.hide();
				},

				info: function(mes, callback){
					var html = '<div class="popup__text">' + mes + '</div>'
						+	'<div class="popup__link"><span class="btn btn_green">Ок</span></div>';
					self.popup.open('.popup_info', html);

					$('.popup_info').find('.btn').click(function(){
						self.popup.close($('.popup_info'));
						if ( callback ) callback();
					});
				},

				remove: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.remove();
				},

				init: function(){
					$('body')
						.on('click', '.popup', function(e){
							if ( !$(e.target).closest('.popup__layout').size() ) self.popup.close('.popup');
						})
						.on('click', '.popup__close, .js-popup-close', function(e){
							e.preventDefault();
							e.stopPropagation();
							self.popup.close($(this).closest('.popup'));
						})
						.on('click', '[data-popup]', function(e){
							e.preventDefault();
							e.stopPropagation();
							self.popup.close('.popup');
							self.popup.open($(this).data('popup'));
						})
					;
				}
			},

			bottomStick: {
				event: function(el){
					$(el).each(function(){
						var t = $(this).offset().top;
						var h = $('body').height();
						$(this).height(h - t);
					});
				},
				init: function(el){
					self.bottomStick.event(el);
					$(window).resize(self.bottomStick.event);
				}
			},

			scrollBar: {

				init: function(el){
					$(el).not('.inited').mCustomScrollbar({
						callbacks:{
							onCreate: function(){
								$(this).addClass('inited');
							}
						}
					});
				}
			}

		},

		tabs: {
			init: function(){
				$('body')
					.on('click', '[data-tab-link]', function(e){
						/**
						 * data-tab-link='name_1', data-tab-group='names'
						 * data-tab-targ='name_1', data-tab-group='names'
						 **/
						e.preventDefault();
						var $t = $(this);
						var group = $t.data('tab-group');
						var $links = $('[data-tab-link]').filter(selectGroup);
						var $tabs = $('[data-tab-targ]').filter(selectGroup);
						var ind = $t.data('tab-link');
						var $tabItem = $('[data-tab-targ='+ind+']').filter(selectGroup);

						if( !$t.hasClass('active')){
							$links.removeClass('active');
							$t.addClass('active');
							$tabs.fadeOut(150);
							setTimeout(function(){
								$tabs.removeClass('active');
								$tabItem.fadeIn(150, function(){
									$(this).addClass('active');
								});
							}, 150)
						}

						function selectGroup(){
							return $(this).data('tab-group') === group;
						}

					})
				;
			}
		},

		init: function() {

			//self.viewPort.init();
			//self.preLoad.init();
			//self.tooltip.init();
			//self.scrollBar.init('.js-scroll-wrap');
			//self.bottomStick.init('.js-bottom-stick');

			//$('[data-required=phone]').mask('+7 (999) 999-99-99');

			var $b = $('body');

			$b
				.on('click', '[data-scrollto]', function(e){
					e.preventDefault();
					var $el = $(this).data('scrollto');
					$('html,body').animate({scrollTop: $($el).offset().top}, 500);
				})

			;
		}
	};
	var self = {};
	var loader = function(){
		self = app.module;
		jQuery.app = app.module;
		app.init();
	};
	var ali = setInterval(function(){
		if (typeof jQuery !== 'function') return;
		clearInterval(ali);
		setTimeout(loader, 0);
	}, 50);

})();



