// Event listenr



$(function(){


	$('body')

		.on('submit', '.js-form', function (e) {
			e.preventDefault();
			var $form = $(this);

			if ($.app.checkForm($form)) {

				$.ajax({
					type: $form.attr('method'),
					url: $form.attr('action'),
					data: $form.serialize(),
					complete: function (res) {

						var data = JSON.parse(res.responseText);

						if (res.status) {
							var status = res.status,
								formName = $form.data('form-name');

							if (status == 200) {    /** success */

								switch (formName) {
									case 'feedback':

										break;

									case 'faq':

										break;
								}

								location.reload();

							} else if (status == 400) {    /** error */

							} else if (status == 500) {    /** Internal Server Error */

							} else {
								/** other trouble */
								console.error(res);
							}

						}
					}
				});
			}

		})


	;

});